"""
Serve webcam images from a Redis store using Tornado.
Usage:
   python server.py
"""
import ast
import base64
import os
import cv2
import numpy as np
from tornado import websocket, web, ioloop, autoreload
from face_classification.src.back_video_emotion_color_demo import process

MAX_FPS = 100


class IndexHandler(web.RequestHandler):
    def get(self):
        self.render('templates/WebcamJS.html')


class LoginHandler(web.RequestHandler):
    def get(self):
        self.render('templates/form/login.html')


class PageHandler(web.RequestHandler):
    def get(self):
        title = str(self.get_argument("title", None, True))
        page = str(self.get_argument("page", None, True))
        print 'page: ' + page + ' title: ' + title
        self.render('templates/pages/' + title + '/' + page + '.html')


class MainHandler(web.RequestHandler):
    def get(self):
        self.render('templates/user/index.html')


class FrameHandler(web.RequestHandler):
    def get(self):
        self.render('templates/user/frame.html')


class SocketHandler(websocket.WebSocketHandler):
    """ Handler for websocket queries. """

    def __init__(self, *args, **kwargs):
        global host, port
        """ Initialize the Redis store and framerate monitor. """
        super(SocketHandler, self).__init__(*args, **kwargs)

    @staticmethod
    def data_uri_to_cv2_img(uri):
        encoded_data = uri.split(',')[1]
        nparr = np.fromstring(encoded_data.decode('base64'), np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        return img

    def on_message(self, message):
        """ Retrieve image ID from database until different from last ID,
        then retrieve image, de-serialize, encode and send to client. """
        j_data = ast.literal_eval(message)

        if str(j_data.get('stat')) == 'ON':
            message = j_data.get('img')
            # print j_data.get('usrname')
            # print j_data.get('lastname')
            im = self.data_uri_to_cv2_img(message)
            bgr_image = process(im, j_data.get('usrname'), j_data.get('title'), str(j_data.get('page')),
                                j_data.get('time_stamp'), 'ON')
            retval, buffer = cv2.imencode('.jpeg', bgr_image)
            image = base64.b64encode(buffer)
            self.write_message(image)
        else:
            bgr_image = process(None, j_data.get('usrname'), '-', '-',
                                j_data.get('time_stamp'), 'OFF')
        # response = self._map_client.send(coils.MapSockRequest('get', 'image'))
        # sio = StringIO.StringIO(bgr_image)
        # image = np.load(sio)
        # print image
        # image = base64.b64encode(bgr_image)
        # sio = StringIO.StringIO(response)
        # image = np.load(sio)
        # image = base64.b64encode(image)
        # self.write_message(image)
        # cv2.imshow('OpenCV', bgr_image)
        # if cv2.waitKey(1) & 0xFF == ord('q'):
        #    print 'q'
        # print(resp)


app = web.Application([
    (r'/', LoginHandler),
    (r'/login', LoginHandler),
    (r'/frame', FrameHandler),
    (r'/pages', PageHandler),
    (r'/main', MainHandler),
    (r'/ws', SocketHandler),
    (r'/js/(.*)', web.StaticFileHandler, {'path': './static/js'}),
    (r'/fonts/(.*)', web.StaticFileHandler, {'path': './static/fonts'}),
    (r'/dist/(.*)', web.StaticFileHandler, {'path': './static/css'}),
    (r'/css/fonts/(.*)', web.StaticFileHandler, {'path': './static/fonts'}),
    (r'/css/(.*)', web.StaticFileHandler, {'path': './static/css'}),
    (r'/images/(.*)', web.StaticFileHandler, {'path': './static/images'}),
    (r'/data/(.*)', web.StaticFileHandler, {'path': './static/data'}),

])

if __name__ == '__main__':
    app.listen(9000)
    autoreload.start()
    for dir, _, files in os.walk('static'):
        [autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]
    for dir, _, files in os.walk('templates'):
        [autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]
    print ('server started at: http://localhost:9000')
    ioloop.IOLoop.instance().start()
